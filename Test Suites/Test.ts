<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>288d0469-763d-4157-918d-86e42a69a6c8</testSuiteGuid>
   <testCaseLink>
      <guid>57118a77-bc9d-46f5-8674-967a588a3478</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/GetListUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>923a6b00-4ad9-4084-b0f1-8ec36e7ba648</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/GetSingleUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>befc5e2f-8c6e-42f4-86d0-49b304529fb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Update</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e901d07c-0918-4d81-9389-2e8ef4fc2d03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PostRegisterSuccessful</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
